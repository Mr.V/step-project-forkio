let gulp = require('gulp');
let sass = require('gulp-sass');
let watch = require('gulp-watch');
let browserSync = require('browser-sync');
let concat = require('gulp-concat');
let uglify = require('gulp-minify-css');


/**
 * Exporting Vitya's and Oleg's styles from scss to css
 **/
function SassDZH() {
    return gulp.src('src/dev_stud_vitya/sass/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/dev_stud_vitya/css'))
        .pipe(browserSync.reload({ stream: true }))
}

function SassBRT() {
    return gulp.src('src/dev_stud_oleg/sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/dev_stud_oleg/css'))
}

gulp.task('sass-dzh', SassDZH);
gulp.task('sass-brt', SassBRT);


/**
 * Automatization saving from scss to css files
 **/
gulp.task('watch', function() {
    gulp.watch('src/dev_stud_vitya/sass/*.scss', SassDZH);
    gulp.watch('src/dev_stud_oleg/sass/*.scss', SassBRT);
    gulp.watch('src/index.html', browserSync.reload)
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: './'
        },

        notify: false
    });
});


/**
 * Minifikation project
 **/
gulp.task('uglify', function() {
    gulp.src('src/dev_stud_vitya/css/*.css')
        .pipe(uglify())
        .pipe(gulp.dest('dist/css'))
});