/**
 * Function for make a hover effect of logotype
 **/
// let $logoHover = $('<img src="src/dev_stud_vitya/img/logo-hover@1X.png" alt="forkio">');
// let $logoUnHover = $('<img src="src/dev_stud_vitya/img/Logo@1X (3).png" alt="forkio">');

let logotypeHover = $(function() {
    let $hover = $('.logotype').hover(function() {
        $('.no-hover').css('display', 'none');
        $('.is-hover').css('display', 'block');

        $('.txt-logo').css('color', 'white');
    });

    $hover.mouseleave(function() {
        $('.no-hover').css('display', 'block');
        $('.is-hover').css('display', 'none');

        $('.txt-logo').css('color', '#8D81AC');
    });
});

// let $logoHover = $('.is-hover');
// let $logoUnHover = $('no-hover');

// $(function() {
//     $('.logotype').hover(function() {
//         $logoUnHover.css('display', 'none');
//         $logoHover.css('display', 'block');

//         $('.txt-logo').css('color', 'white');
//     });

//     $('.logotype').mouseleave(function() {
//         $logoUnHover.css('display', 'block');
//         $logoHover.css('display', 'none');

//         $('.txt-logo').css('color', '#8D81AC');
//     });
// })

/**
 * Function for make the drop down menu for mobile-version
 **/
$('.drop-down-menu-exelent').click(function() {
    $('.window-drop-menu').css('display', 'block');
    $('.delete').remove();
    $('.download-free').css('marginTop', '299px');
});

document.addEventListener('click', function(event) {
    if (event.target.classList.contains('target-menu')) {
        $('.target-menu.active').removeClass('active');
        console.log(event.target);
        $(event.target).addClass('active');
    }
});